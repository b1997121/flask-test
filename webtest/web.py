#! /usr/bin/env python

from __future__ import print_function

import random
import database
import subprocess

from subprocess import CalledProcessError
from flask import Flask, render_template
from flask import request

app = Flask(__name__)

@app.route('/')
def index():
    db = database.Database()
    return_result = db.query('SELECT DISTINCT group_name FROM members')
    return render_template(
        'index.html',
        index=True,
        option=return_result
    )

@app.route('/draw', methods=['POST'])
def draw():
    db = database.Database()
    group_name = request.form.get('group_name', 'ALL')
    q_text = 'SELECT id FROM members'
    if group_name != 'ALL':
        q_text += ' WHERE group_name = "{}"'.format(group_name)
    return_data = db.query(q_text)
    valid_member_ids = [row[0] for row in return_data]

    if not valid_member_ids:
        err_msg = '<p>No members in group {}</p>'.format(group_name)
        return err_msg, 404

    lucky_member_id = random.choice(valid_member_ids)

    db.insert_draw_data(lucky_member_id)

    q_text = 'SELECT * FROM members WHERE id = {}'.format(lucky_member_id)
    return_data = db.query(q_text)
    #show_text = ''
    #show_text += '<p> Name: {}, location: {} </p>'.format(return_data[0].name, return_data[0].group_name)
    #return show_text
    return render_template(
        'draw.html',
        name=return_data[0][0],
        group=return_data[0][1],
    )

@app.route('/history')
def history():
    db = database.Database()
    recent_histories = db.query(
        'SELECT m.name, m.group_name, d.time '
        'FROM draw_histories AS d, members as m '
        'WHERE m.id = d.member_id '
        'ORDER BY d.time DESC '
    )
    return render_template(
        'history.html',
        recent_histories=recent_histories
    )

@app.route('/reset')
def reset():
    db = database.Database()
    return_result = db.query('DELETE FROM draw_histories')
    if return_result.rowcount == 0:
        result = 'PASS'
    else:
        result = 'FAIL'
    return render_template(
        'reset.html',
        result=result
    )

@app.route('/add_page')
def add_page():
    return render_template(
        'index.html',
        index=False
    )

@app.route('/add', methods=['POST'])
def add():
    db = database.Database()
    name = request.form.get('name')
    group_name = request.form.get('group_name')
    if name and group_name:
        result = db.insert_member_data(name=name, group_name=group_name)
    else:
        result = 'Please input all textbox'
    return render_template(
        'add.html',
        result=result
    )

@app.route('/ipmitool')
def ipmitool_html():
    return render_template(
        'ipmitool.html'
    )


@app.route('/ipmitool', methods=['POST'])
def ipmitool_process():
    result = 'No BMC IP'
    req_form = request.form
    bmc_ip = req_form.get('bmc_ip', None)
    bmc_usr = req_form.get('bmc_usr', 'admin')
    bmc_pwd = req_form.get('bmc_pwd', 'admin')
    run_type = req_form.get('run_type', 'status')
    
    print('Request form: {}'.format(req_form))

    if bmc_ip:
        cmd = 'ipmitool -H {H} -U {U} -P {P} chassis power status'.format(H=bmc_ip, U=bmc_usr, P=bmc_pwd)
        print('commnad: {}'.format(cmd))
        try:
            result = subprocess.check_output(cmd, shell=True)
        except CalledProcessError:
            result='Cannot execute command "{}"'.format(cmd)

    return render_template(
        'ipmitool.html',
        result=result
    )
    '''
    result = 'No BMC IP'
    req_args = request.args
    bmc_ip = req_args.get('bmc_ip', None)
    bmc_usr = req_args.get('bmc_usr', 'admin')
    bmc_pwd = req_args.get('bmc_pwd', 'admin')
    print('req_args: {}'.format(req_args))
    print('bmc_ip: {}'.format(bmc_ip))
    print('bmc_usr: {}'.format(bmc_usr))
    print('bmc_pwd: {}'.format(bmc_pwd))
    if bmc_ip:
        cmd = 'ipmitool -H {H} -U {U} -P {P} chassis power status'.format(H=bmc_ip, U=bmc_usr, P=bmc_pwd)
        result = subprocess.check_output(cmd, shell=True)
    return '<p> {R}'.format(R=result)
    '''

if __name__ == '__main__':
    app.run(debug=True, host='192.168.93.65')
