#! /usr/bin/env python

import re
from sqlalchemy import create_engine, Column, Integer, String, ForeignKey, DateTime, func, text
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship, backref
from sqlalchemy.sql import text

engine = create_engine('mysql+pymysql://localuser:123456@localhost/webtest', echo=False)
Base = declarative_base()

class Member(Base):
    __tablename__ = 'members'
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(50), nullable=False)
    group_name = Column(String(50), nullable=False)
    def __repr__(self):
        return "<User(name='%s', group='%s')>" % (self.name, self.group_name)

class Draw_History(Base):
    __tablename__ = 'draw_histories'
    id = Column(Integer, primary_key=True, autoincrement=True)
    member_id = Column(Integer, ForeignKey('members.id'), nullable=False)
    time = Column(DateTime, default=func.now())
    #member = relationship(Member, back_populates='draw_histories')
    member = relationship(
        Member,
        backref=backref('draw_histories',
                         uselist=True,
                         cascade='delete,all'))
    def __repr__(self):
        return "<Address(time='%s')>" % (self.time)

class Database(object):

    def __init__(self):
        session_create = sessionmaker(bind=engine)
        self.session = session_create()
        self.conn = engine.connect()

    def _query_member_db(self, q_text):
        return self.session.query(Member).from_statement(text(q_text)).all()

    def _query_draw_db(self, q_text):
        return self.session.query(Draw_History).from_statement(text(q_text)).all()

    def query(self, input_text):
        q_text = text(input_text)
        if not re.search('^delete|^drop|^TRUNCATE', input_text, re.I):
            result = self.conn.execute(q_text).fetchall()
        else:
            result = self.conn.execute(q_text)
        return result

    @staticmethod
    def create_table(class_name, table_name):
        if not engine.dialect.has_table(engine, table_name):
            print('Create "{}" table'.format(table_name))
            class_name().__table__.create(bind = engine)

    def get_draw_data(self, key_data):
        return self.session.query(Draw_History).filter_by(member_id=key_data).all()

    def insert_draw_data(self, input_id):
        before_count = len(self.get_draw_data(input_id))
        data = Draw_History(member_id=input_id)
        self.session.add(data)
        self.session.flush()
        self.session.commit()
        after_count = len(self.get_draw_data(input_id))
        if before_count == after_count:
            print('Insert data failed')
        else:
            print('Insert data successfully')

    def get_member_data(self, key_data):
        return self.session.query(Member).filter_by(name=key_data).first()

    def insert_member_data(self, **kwargs):
        return_data = self.get_member_data(kwargs['name'])
        if not return_data:
            data = Member(name=kwargs['name'], group_name=kwargs['group_name'])
            print('Insert data {}'.format(data))
            self.session.add(data)
            self.session.flush()
            self.session.commit()
            return_data = self.get_member_data(kwargs['name'])
            if not return_data:
                result = 'Insert data failed'
            else:
                result = 'Insert data successfully'
        else:
            result = '{} is already in database'.format(kwargs['name'])
        print(result)
        return result

    def create_all(self):
        self.create_table(Member, 'members')
        self.create_table(Draw_History, 'draw_histories')
        self.insert_member_data(name='Det', group_name='A')
        self.insert_member_data(name='Renee', group_name='C')
        self.insert_member_data(name='Brian', group_name='A')
        self.insert_member_data(name='Chying', group_name='C')
        self.insert_member_data(name='Triple', group_name='C')

def main():
    db = Database()
    db.create_all()

if __name__ == '__main__':
    main()
